﻿#include <iostream>
#include <cmath>
class MyClass
{
private:
	int a;
public:
	int GetA()
	{
     return a;
	}	
	void SetA(int newA)
	{
		a = newA;
		std::cout << a;
	}
};

class vector
{
private:
	double x;
	double y;
	double z;

public:
	vector(double _x,double _y,double _z):x(_x), y(_y), z(_z)
	{}
	double vec()
	{
		return sqrt(x*x+y*y+z*z);
	}
	void Show()
	{
		std::cout << '\n' << x << ' ' << y << ' ' << z<<'\n';
	}
};

int main()
{
	MyClass temp;
	temp.SetA(5);

	vector v(5,5,5);
	v.vec();
	v.Show();
	std::cout << v.vec();
}

